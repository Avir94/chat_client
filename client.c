/*
 * Modified code from http://stackoverflow.com/questions/20588108/chat-client-in-c
 * Combined with code from http://www.linuxhowtos.org/C_C++/socket.htm
 * also http://forums.codeguru.com/showthread.php?519765-Async-chat-server-client-with-select%28%29
 * 
 * Author: Connor Riva
 *
 * Worked with: Mason Neal
 *
 * Comments: first message doesnt send, everything else does
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <readline/readline.h>
#include <fcntl.h>

int sock;

void error(const char *msg){
    perror(msg);
    exit(0);
}

void handle_line(char* ch){

    if (strlen(ch) == 1)
	return;

    strcat(ch, "\n");
    if(write(sock, ch, strlen(ch)) < 0)
	printf("error writing.\n");
    free(ch);
}

int main(int argc, char ** argv){
    
    int port, n, SelectResults, buffSize, RecvBytes, SentBytes;
    int index = 0;
    int i = 0;
    
    struct sockaddr_in servAddr;
    struct hostent     *server;
    char               Sentbuff[256]; 
    char               Recvbuff[256];
    const char const*  prompt = "> ";
    fd_set	       fdread;
    fd_set	       BackUpfdread;

    

    port = 49153;                                // For our specific usage, we dont need the user to specify port number 
    sock = socket(AF_INET, SOCK_STREAM, 0);      // Set up socket
    if (sock < 0)
	error("encountered an error opening the socket");

    server = gethostbyname("localhost");         // Costruct server, for our specific usage, we dont need the user to specify host
    if (server == NULL)                          // Checks to see if server can be connected to
	error("encountered an error finding host");

    bzero((char *) &servAddr, sizeof(servAddr)); // set the sockaddr to zero 
    servAddr.sin_family = AF_INET;               // determining address family for Internet Protocal

    bcopy((char *)server->h_addr, 
	  (char *)&servAddr.sin_addr.s_addr,
	  server->h_length);                     // takes the host's address and sets it to the servAdder's address

    servAddr.sin_port = htons(port);             // host to network short 

    buffSize = sock;

    // Connect to server
    if (connect(sock, 
		(struct sockaddr *) &servAddr, 
		sizeof(servAddr)) < 0) {         // Checking if connection was successful
	error("connect failed");
    }
  
    printf("Please, enter your name.\n");
    
    rl_callback_handler_install(prompt, handle_line);

    // Clear the fd sets
    FD_ZERO(&fdread);
    FD_ZERO(&BackUpfdread);
    // Asign ConnectSocket into fdread and fdwrite.
    FD_SET(sock, &fdread);
    FD_SET(fileno(rl_instream), &fdread);

    while(1){                                    // Continuously read and write from the server until quit program       
	
	memcpy(&BackUpfdread, &fdread, sizeof(&fdread));

        SelectResults = select(sock+1 ,&BackUpfdread,NULL,NULL,NULL);
	
	// Something to read from server.
	if(FD_ISSET(sock,&BackUpfdread)){
	    RecvBytes = read(sock, Recvbuff, sizeof(Recvbuff));
	    if(RecvBytes > 0)
		{
		    Recvbuff[RecvBytes] = 0; // clearing the buffer 
		    rl_save_prompt();        // saving what we have been typing
		    rl_message(Recvbuff);    // display the server message
		    rl_clear_message();      // clear the screen so we can keep typing
		    rl_restore_prompt();     // restore our prompt
		}
	}
	
        if(FD_ISSET(fileno(rl_instream), &BackUpfdread))
	    rl_callback_read_char();
    }
}
